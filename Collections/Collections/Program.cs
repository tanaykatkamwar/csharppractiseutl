﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            //create a collection
            var ListOfSuperHeroes = new List<SuperHero>();
            var ListOfSuperHeroes2 = new List<SuperHero>();
            var FinalList = new List<SuperHero>();
            

            ListOfSuperHeroes = GetFiveSuperHeroes();
            
            var temphero1 = new SuperHero();
            temphero1.age = 22;
            temphero1.brand = "Apple";
            temphero1.power = "Coding";
            temphero1.name = "Jay";
            var temphero2 = new SuperHero();
            temphero2.age = 21;
            temphero2.brand = "Apple";
            temphero2.power = "Coding";
            temphero2.name = "vijay";
            ListOfSuperHeroes2.Add(temphero1);
            ListOfSuperHeroes2.Add(temphero2);
            //ShowSuperHeroes(ListOfSuperHeroes);
            //ShowSuperHeroes(ListOfSuperHeroes2);
            FinalList.AddRange(ListOfSuperHeroes);
            FinalList.AddRange(ListOfSuperHeroes2);
            ShowSuperHeroes(FinalList);
            int age = 21;
            String name = "Pankaj";
            SuperHero tempheroo = Findbyname(name, FinalList);
            SuperHero returnhero = findbyage(age,FinalList);
            Console.ReadLine();
        }

        private static SuperHero Findbyname(string name, List<SuperHero> finalList)
        {
            SuperHero tempheroo = new SuperHero();
            tempheroo = finalList.Select(x => x).Where(x => x.name == name).FirstOrDefault();
            return tempheroo;
        }

        private static SuperHero findbyage(int age, List<SuperHero> finalList)
        {
            SuperHero temphero = new SuperHero();
            // var temphero = listOfSuperHeroes.Select(x => x).Where(x => x.age == 10);
            temphero = finalList.Select(x => x).Where(x => x.age == age).FirstOrDefault();
            return temphero;
        }

        private static void ShowSuperHeroes(List<SuperHero> listOfSuperHeroes)
        {
            //.OrderByDescending(x => x.Delivery.SubmissionDate);
            // listOfSuperHeroes.OrderByDescending(x => x.age);
            //listOfSuperHeroes.OrderByDescending(x => x.age);
            listOfSuperHeroes = listOfSuperHeroes.OrderByDescending(x => x.age).ToList();
            foreach (var x in listOfSuperHeroes)
            {
                Console.WriteLine("------------------");
                Console.WriteLine("Name " + x.name);
                Console.WriteLine("Age " + x.age);
                Console.WriteLine("Power  " + x.power);
                Console.WriteLine("Brand " + x.brand);
                Console.WriteLine("------------------");
            }
        }

        private static List<SuperHero> GetFiveSuperHeroes()
        {
            //create a n empty list.
            var tempList = new List<SuperHero>();

            //five super heroes. 
            var name = "";
            var power = "";
            var brand = "";
            var age = 0;

            //five super heroes. 
            var tempHero = new SuperHero();
            var tempHero2 = new SuperHero();
            var tempHero3 = new SuperHero();
            var tempHero4 = new SuperHero();
            var tempHero5 = new SuperHero();
            var tempHero6 = new SuperHero();

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 50;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);


            name = "Jay";
            power = "Money";
            brand = "Apple";
            age = 24;

            tempHero2.name = name;
            tempHero2.power = power;
            tempHero2.brand = brand;
            tempHero2.age = age;

            //add this hero to list. 
            tempList.Add(tempHero2);

            name = "tanay";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero3.name = name;
            tempHero3.power = power;
            tempHero3.brand = brand;
            tempHero3.age = age;

            //add this hero to list. 
            tempList.Add(tempHero3);

            name = "pankaj";
            power = "Money";
            brand = "DC";
            age = 78;

            tempHero4.name = name;
            tempHero4.power = power;
            tempHero4.brand = brand;
            tempHero4.age = age;

            //add this hero to list. 
            tempList.Add(tempHero4);

            name = "soumik";
            power = "Money";
            brand = "DC";
            age = 56;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);

            name = "prajit";
            power = "Money";
            brand = "DC";
            age = 47;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);


            return tempList;
        }
    }
}
