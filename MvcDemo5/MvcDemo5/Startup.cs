﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MvcDemo5.Startup))]
namespace MvcDemo5
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
