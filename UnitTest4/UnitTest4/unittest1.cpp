#include "stdafx.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest4
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		int fact(int n) {
			if (n == 1)
				return 1;
			else
				return n * fact(n - 1);
		}
		
		TEST_METHOD(TestMethod1)
		{
			int iExpected = 120;
			int iResult = fact(5);
			Assert::AreEqual(iExpected, iResult);

			// TODO: Your test code here
		}

	};
}