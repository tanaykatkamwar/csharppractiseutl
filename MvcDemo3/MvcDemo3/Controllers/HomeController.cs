﻿using MvcDemo3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcDemo3.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult ShowList3()
        {
            ViewBag.Message = "This is the list of students";
            var ListOfStudents = new List<Student>();
            ListOfStudents = GetFiveStudents();
            //return View(db.NewsModel.ToList());
            return View(ListOfStudents);
            //return View();
        }
        public ActionResult ShowList4()
        {
            ViewBag.Message = "This is the list";
            var ListOfSuperHeroes = new List<SuperHero>();

            ListOfSuperHeroes = GetFiveSuperHeroes();


            //return View(db.NewsModel.ToList());
            return View(ListOfSuperHeroes);
        }
        private static List<SuperHero> GetFiveSuperHeroes()
        {
            //create a n empty list.
            var tempList = new List<SuperHero>();

            //five super heroes. 
            var name = "";
            var power = "";
            var brand = "";
            var age = 0;

            //five super heroes. 
            var tempHero = new SuperHero();
            var tempHero2 = new SuperHero();
            var tempHero3 = new SuperHero();
            var tempHero4 = new SuperHero();
            var tempHero5 = new SuperHero();
            var tempHero6 = new SuperHero();

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 50;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);


            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero2.name = name;
            tempHero2.power = power;
            tempHero2.brand = brand;
            tempHero2.age = age;

            //add this hero to list. 
            tempList.Add(tempHero2);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero3.name = name;
            tempHero3.power = power;
            tempHero3.brand = brand;
            tempHero3.age = age;

            //add this hero to list. 
            tempList.Add(tempHero3);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero4.name = name;
            tempHero4.power = power;
            tempHero4.brand = brand;
            tempHero4.age = age;

            //add this hero to list. 
            tempList.Add(tempHero4);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);


            return tempList;
        }
        private static List<Student> GetFiveStudents()
        {
            //create a n empty list.
            var tempList = new List<Student>();

            //five super heroes. 
            var name = "";
            var age = 0;

            //five super heroes. 
            var tempHero = new Student();
            var tempHero2 = new Student();
            var tempHero3 = new Student();
            var tempHero4 = new Student();
            var tempHero5 = new Student();
            var tempHero6 = new Student();

            name = "Tanay";
            age = 22;

            tempHero.name = name;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);


            name = "Pankaj";
            
            age = 48;

            tempHero2.name = name;
           
            tempHero2.age = age;

            //add this hero to list. 
            tempList.Add(tempHero2);

            name = "Soumik";
            
            age = 45;

            tempHero3.name = name;
            
            tempHero3.age = age;

            //add this hero to list. 
            tempList.Add(tempHero3);

            name = "Ram";
            
            age = 34;

            tempHero4.name = name;
            
            tempHero4.age = age;

            //add this hero to list. 
            tempList.Add(tempHero4);

            name = "Sham";
            
            age = 12;

            tempHero5.name = name;
           
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);

            name = "Ritesh";
           
            age = 23;

            tempHero5.name = name;
           
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);


            return tempList;
        }
    }
}