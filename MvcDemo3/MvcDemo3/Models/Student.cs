﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcDemo3.Models
{
    public class Student
    {
        public String name { get; set; }
        public int age { get; set; }
    }
}