﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sweetshop2
{

    //the primary class we will use thoroughout the project. 
    class Sweets
    {
        //we need max customer count
        //we will take this from user.

        private int maxcustomercount {set;get;}

        //price of sweet
        private int priceofeachsweet { set; get; }

        //revenue for the day
        private int revenuefortheday { set; get; }

        //set the maxstock
        private int maxstock { set; get; }

        //set totalsweets sold 
        private int totalsweetssold { set; get; }


       //set max customer count.
       public void setmaxcustomercount(int customercount)
        {
            maxcustomercount = customercount;
        }

        public void setmaxstock(int stock)
        {
            maxstock = stock;
        }

        //set revenue for day. 
        public void setrevenuefortheday(int revenue)
        {
            revenuefortheday = revenue;
        }

        public int gettotalsweetssold()
        {
            return totalsweetssold;
        }

        public int getpersweetprice()
        {
            return priceofeachsweet;
        }

        public int gettotalrevenueforday()
        {
            return revenuefortheday;
        }

        public int getremainingstock()
        {
            return maxstock;
        }

        public void ShowSummary(int totalcustomers, int totalsweetssold, int persweetprice, int totalrevenueforday, int remainingstock)
        {
            var message = "";

            message = "----------------------FINAL REPORT-------------------------";
            showmessage(message);
            message = "Total Customers - " + totalcustomers;
            showmessage(message);
            message = "Total Sweets Sold - " + totalsweetssold;
            showmessage(message);
            message = "Per Sweet Price - " + persweetprice;
            showmessage(message);
            message = "Total Revenue for the day - " + totalrevenueforday;
            showmessage(message);
            message = "Remaining stock - " + remainingstock;
            showmessage(message);
            message = "----------------------Thank you for buying so many sweets.-------------------------";
            showmessage(message);
        }

        //set price of each sweet.
        public void setpriceofeachsweet(int sweetprice)
        {
            priceofeachsweet = sweetprice;
        }

        public int getnumberofcustomers()
        {
            return maxcustomercount;
        }

        public void ProcessAllTheCustomers()
        {
            for(int i=0;i<maxcustomercount;i++)
            {
                var message = "Enter number of sweets for customer (max 40 sweets) - " + (i + 1);
                var numberofsweets = collectinput(message);
                updatestockandrevenue(numberofsweets);
            }
        }

        private void updatestockandrevenue(int numberofsweets)
        {
            //check if number of sweets is less than stock.
            //and accordingly reduce stock
            if(numberofsweets<maxstock)
            {
                //reduce stock with number of sweets
                maxstock = maxstock - numberofsweets;
            }
            else
            {
                //first increase stock
                maxstock = maxstock + 50;

                maxstock = maxstock - numberofsweets;
            }

            //now calculate revenue and update revenue.
            int temprevenueforthiscustomer = priceofeachsweet * numberofsweets;
            revenuefortheday = revenuefortheday + temprevenueforthiscustomer;

            //finally update total sweets sold.
            totalsweetssold = totalsweetssold + numberofsweets;
        }

        //to collect input
        public int collectinput(string message)
        {
            Console.WriteLine(message);
            var input = Console.ReadLine();
            var collectednumber = Convert.ToInt32(input);
            return collectednumber;
        }

        public void showmessage(string message)
        {
            Console.WriteLine(message);
        }

    }
}
