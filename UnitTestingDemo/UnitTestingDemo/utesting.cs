﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestingDemo
{
    class utesting

    {

        ////unit test for code reuse.

        //copy paste this code file to build new unit tests quickly.

        [TestClass]

        public class SampleTest

        {

            [TestMethod]

            public void TestSampleOne()

            {

                var actual = true;

                var expected = true; Assert.AreEqual(expected, actual);

            }

            [TestMethod]

            public void TestSampleTwo()

            {

                var actual = true;

                var expected = false;

                Assert.AreEqual(expected, actual);

            }

            [TestMethod]

            public void TestSampleThree()

            {

                var ip1 = 8305.89;

                var ip2 = 60895.98;

                var actual = 69201.87;

                // var expected = ip1+ip2;

                var expected = Program.sumOfTwoNumber(ip1, ip2);

                Assert.AreEqual(expected, actual);

            }


            [TestMethod]

            public void TestSampleSix()

            {
                var tempHero5 = new SuperHero();
                string name = "Batman";
                string power = "Money";
                string brand = "DC";
                int age = 30;

                tempHero5.name = name;
                tempHero5.power = power;
                tempHero5.brand = brand;
                tempHero5.age = age;

                //assign our expected value. 
                var expectedhero = tempHero5;

                //get our actual value from the unit we are testing.
                //we are testing the unit ReturnSpecificHeroBasedOnAge
                //create a collection
                var ListOfSuperHeroes = new List<SuperHero>();
                ListOfSuperHeroes = Program.GetFiveSuperHeroes();
                int ageofhero = 30;
                SuperHero SpecificAgeSuperHero = Program.ReturnSpecificHeroBasedOnAge(ListOfSuperHeroes, ageofhero);
                var actualhero = SpecificAgeSuperHero;

                //okay set expected as true bool value. 
                bool expected = true;
                //in the beginning set actual value to false. 
                //if our test passes we will change it to true.
                bool actual = false;
                if (expectedhero.age == actualhero.age &&
                   expectedhero.brand == actualhero.brand &&
                   expectedhero.name == actualhero.name &&
                   expectedhero.power == actualhero.power)
                {
                    //all four values matching means, our unit is working correctly. 
                    //set actual bool to true. 
                    actual = true;
                }

                Assert.AreEqual(expected, actual);

            }
            [TestMethod]
            public void TestSampleSeven()

            {
                var tempHero5 = new SuperHero();
                string name = "Batman";
                string power = "Money";
                string brand = "Marvel";
                int age = 30;

                tempHero5.name = name;
                tempHero5.power = power;
                tempHero5.brand = brand;
                tempHero5.age = age;

                //assign our expected value.
                var expectedhero = tempHero5;

                //get our actual value from the unit we are testing.
                //we are testing the unit ReturnSpecificHeroBasedOnAge
                //create a collection
                var ListOfSuperHeroes = new List<SuperHero>();
                ListOfSuperHeroes = Program.GetFiveSuperHeroes();
                int ageofhero = 30;
                SuperHero SpecificAgeSuperHero = Program.ReturnSpecificHeroBasedOnAge(ListOfSuperHeroes, ageofhero);
                var actualhero = SpecificAgeSuperHero;

                //okay set expected as true bool value.
                bool expected = true;
                //in the beginning set actual value to false.
                //if our test passes we will change it to true.
                bool actual = false;
                if (expectedhero.age == actualhero.age &&
                    expectedhero.brand == actualhero.brand &&
                    expectedhero.name == actualhero.name &&
                    expectedhero.power == actualhero.power)
                {
                    //all four values matching means, our unit is working correctly.
                    //set actual bool to true.
                    actual = true;
                }

                Assert.AreEqual(expected, actual);

            }

        }

    }
}
