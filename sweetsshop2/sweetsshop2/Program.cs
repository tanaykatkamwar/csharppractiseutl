﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sweetsshop2
{
    class Program
    {
        static void Main(string[] args)
        {
            //lets put all sweet stuff in a separate function
            //keep main clean.

            ProcessSweetStuff();

            Console.ReadLine();
        }

        private static void ProcessSweetStuff()
        {
            //lets get a sweet object
            var tempSweetStuff = new Sweets();

            tempSweetStuff = SetEssentials(tempSweetStuff);

            tempSweetStuff = ProcessCustomers(tempSweetStuff);

            DisplaySummary(tempSweetStuff);


        }

        //display end of day summary
        private static void DisplaySummary(Sweets tempSweetStuff)
        {
            //get all items that can be displayed

            //display total customers
            int totalcustomers = tempSweetStuff.getnumberofcustomers();
            //display total sweets sold
            int totalsweetssold = tempSweetStuff.gettotalsweetssold();
            //display per sweet price
            int persweetprice = tempSweetStuff.getpersweetprice();
            //display total revenue
            int totalrevenueforday = tempSweetStuff.gettotalrevenueforday();
            //display remaining stock
            int remainingstock = tempSweetStuff.getremainingstock();

            tempSweetStuff.ShowSummary(totalcustomers, totalsweetssold, persweetprice, totalrevenueforday, remainingstock);
        }


        //process what each customer wants. 
        private static Sweets ProcessCustomers(Sweets tempSweetStuff)
        {
            //okay, process the number of customers. each customer will ask for x number of sweets. 
            //get the number of customers. 
            int numberofcustomers = tempSweetStuff.getnumberofcustomers();
            var message = "There are a total of " + numberofcustomers + " customers";

            tempSweetStuff.ProcessAllTheCustomers();

            return tempSweetStuff;
        }

        //setting the essential values. 
        private static Sweets SetEssentials(Sweets tempSweetStuff)
        {
            //set the essential values.
            int sweetprice = 10;
            tempSweetStuff.setpriceofeachsweet(sweetprice);
            int maxstock = 50;
            tempSweetStuff.setmaxstock(maxstock);
            //get and set number of customer for the day. 
            var message = "enter number of customers for today";
            var tempcustomersfortheday = tempSweetStuff.collectinput(message);
            tempSweetStuff.setmaxcustomercount(tempcustomersfortheday);

            return tempSweetStuff;
        }
    }
}
